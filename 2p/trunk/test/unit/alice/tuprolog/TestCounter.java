package alice.tuprolog;

import org.junit.Ignore;

@Ignore
public class TestCounter {
	
	private int value = 0;
	
	public void update() {
		value++;
	}
	
	public int getValue() {
		return value;
	}

}
