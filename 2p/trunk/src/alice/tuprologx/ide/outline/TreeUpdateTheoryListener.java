/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alice.tuprologx.ide.outline;

import alice.tuprolog.event.TheoryEvent;
import alice.tuprolog.event.TheoryListener;

public class TreeUpdateTheoryListener implements TheoryListener {
    private final PrologOutlineTree tree;

	public TreeUpdateTheoryListener(PrologOutlineTree tree) {
            this.tree = tree;
	}

	@Override
	public void theoryChanged(TheoryEvent e) {
		PrologTreeNode root = new PrologTreeNode("root");
		root.setSortable(true);
                root.setSorted(true);
                
		String newTheory = e.getNewTheory().toString();
		String[] lines = newTheory.split("\n");
                for (String line : lines) {
                    if(line.length()>0 && Character.isLetter(line.charAt(0)) && line.replace(" ", "").contains(":-")){
                        String newTerm = line.replace(" ", "").split(":-")[0].split("\\)")[0];
                        String name = newTerm.split("\\(")[0];
                        String[] params = new String[0];
                        if(newTerm.contains("("))
                            params = newTerm.replace(name + "", "").split(",");
                        PrologTreeNode node = new PrologTreeNode(name + "/" + params.length);
                        node.setStartOffset(() -> newTheory.indexOf(line));
                        node.setEndOffset(() -> node.getStartOffset() + name.length());
                        node.setSortable(true);
                        node.setSorted(true);
                        root.add(node);
                    }
                }
                
                root.refresh();
                tree.update(root);
	}
}
