/*
 * 04/07/2012
 *
 * Copyright (C) 2012 Robert Futrell
 * robert_futrell at users.sourceforge.net
 * http://fifesoft.com/rsyntaxtextarea
 *
 * This library is distributed under a modified BSD license.  See the included
 * RSTALanguageSupport.License.txt file for details.
 */
package alice.tuprologx.ide.outline;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.BorderFactory;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;

import org.fife.ui.rsyntaxtextarea.DocumentRange;
import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;
import org.fife.ui.rsyntaxtextarea.RSyntaxUtilities;


/**
 * A tree view showing the outline of XML, similar to the "Outline" view in
 * Eclipse.  It also uses Eclipse's icons, just like the rest of this code
 * completion library.<p>
 *
 * You can get this tree automatically updating in response to edits in an
 * <code>RSyntaxTextArea</code> with {@link XmlLanguageSupport} installed by
 * calling {@link #listenTo(RSyntaxTextArea)}.  Note that an instance of this
 * class can only listen to a single editor at a time, so if your application
 * contains multiple instances of RSyntaxTextArea, you'll either need a separate
 * <code>XmlOutlineTree</code> for each one, or call <code>uninstall()</code>
 * and <code>listenTo(RSyntaxTextArea)</code> each time a new RSTA receives
 * focus.
 * 
 * @author Robert Futrell
 * @version 1.0
 */
public class PrologOutlineTree extends AbstractSourceTree {

	private XmlEditorListener listener;
	private DefaultTreeModel model;
	private PrologTreeCellRenderer xmlTreeCellRenderer;


	/**
	 * Constructor.  The tree created will not have its elements sorted
	 * alphabetically.
	 */
	public PrologOutlineTree() {
		this(false);
	}


	/**
	 * Constructor.
	 *
	 * @param sorted Whether the tree should sort its elements alphabetically.
	 *        Note that outline trees will likely group nodes by type before
	 *        sorting (i.e. methods will be sorted in one group, fields in
	 *        another group, etc.).
	 */
	public PrologOutlineTree(boolean sorted) {
		setSorted(sorted);
		setBorder(BorderFactory.createEmptyBorder(0,8,0,8));
		setRootVisible(false);
		xmlTreeCellRenderer = new PrologTreeCellRenderer();
		setCellRenderer(xmlTreeCellRenderer);
		model = new DefaultTreeModel(new PrologTreeNode("Nothing"));
		setModel(model);
		listener = new XmlEditorListener();
		addTreeSelectionListener(listener);
	}


//static int expandCount;
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void expandInitialNodes() {

		//long start = System.currentTimeMillis();
		//expandCount = 0;
		fastExpandAll(new TreePath(getModel().getRoot()), true);
		//long end2 = System.currentTimeMillis();
		//System.out.println("Expand all all: " + ((end2-start)/1000f) + " seconds (" + expandCount + ")");
		//System.out.println("--- " + getRowCount());

	}


	private void gotoElementAtPath(TreePath path) {
		Object node = path.getLastPathComponent();
		if (node instanceof PrologTreeNode) {
			PrologTreeNode xtn = (PrologTreeNode)node;
			DocumentRange range = new DocumentRange(xtn.getStartOffset(),
					xtn.getEndOffset());
			RSyntaxUtilities.selectAndPossiblyCenter(textArea, range, true);
		}
	}


	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean gotoSelectedElement() {
		TreePath path = getLeadSelectionPath();//e.getNewLeadSelectionPath();
		if (path != null) {
			gotoElementAtPath(path);
			return true;
		}
		return false;
	}


	/**
	 * {@inheritDoc}
	 */
	@Override
	public void listenTo(RSyntaxTextArea textArea) {

		if (this.textArea!=null) {
			uninstall();
		}

		// Nothing new to listen to
		if (textArea==null) {
			return;
		}

		// Listen for future language changes in the text editor
		this.textArea = textArea;
		textArea.addPropertyChangeListener(RSyntaxTextArea.SYNTAX_STYLE_PROPERTY, listener);
	}


	/**
	 * {@inheritDoc}
	 */
	@Override
	public void uninstall() {
		if (textArea!=null) {
			textArea.removePropertyChangeListener(
					RSyntaxTextArea.SYNTAX_STYLE_PROPERTY, listener);
			textArea = null;
		}

	}


	public void update(PrologTreeNode root) {
		if (root!=null) {
			root = (PrologTreeNode)root.cloneWithChildren();
		}
		model.setRoot(root);
		if (root!=null) {
			root.setSorted(isSorted());
		}
		refresh();
	}


	/**
	 * Overridden to update the cell renderer
	 */
	@Override
	public void updateUI() {
		super.updateUI();
		xmlTreeCellRenderer = new PrologTreeCellRenderer();
		setCellRenderer(xmlTreeCellRenderer); // So it picks up new LAF's properties
	}


	/**
	 * Listens for events this tree is interested in (events in the associated
	 * editor, for example), as well as events in this tree.
	 */
	private class XmlEditorListener implements PropertyChangeListener,
							TreeSelectionListener {

		/**
		 * Called whenever the text area's syntax style changes, as well as
		 * when it is re-parsed.
		 */
		public void propertyChange(PropertyChangeEvent e) {

			String name = e.getPropertyName();

                        PrologTreeNode root = (PrologTreeNode)e.getNewValue();
                        update(root);

		}

		/**
		 * Selects the corresponding element in the text editor when a user
		 * clicks on a node in this tree.
		 */
		public void valueChanged(TreeSelectionEvent e) {
			if (getGotoSelectedElementOnClick()) {
				//gotoSelectedElement();
				TreePath newPath = e.getNewLeadSelectionPath();
				if (newPath!=null) {
					gotoElementAtPath(newPath);
				}
			}
		}

	}


	/**
	 * Whenever the caret moves in the listened-to RSyntaxTextArea, this
	 * class ensures that the XML element containing the caret position is
	 * focused in the tree view after a small delay.
	 */
/*
 * TODO: Make me work for any LanguageSupport (don't synchronize if waiting on
 * a pending parse) and pull me out and make me available for all languages.
	private class Synchronizer implements CaretListener, ActionListener {

		private Timer timer;
		private int dot;

		public Synchronizer() {
			timer = new Timer(650, this);
			timer.setRepeats(false);
		}

		public void actionPerformed(ActionEvent e) {
			recursivelyCheck(root);
			//System.out.println("Here");
		}

		public void caretUpdate(CaretEvent e) {
			this.dot = e.getDot();
			timer.restart();
		}

		private boolean recursivelyCheck(XmlTreeNode node) {
			if (node.containsOffset(dot)) {
				for (int i=0; i<node.getChildCount(); i++) {
					XmlTreeNode child = (XmlTreeNode)node.getChildAt(i);
					if (recursivelyCheck(child)) {
						return true;
					}
				}
				// None of the children contain the offset, must this guy
				node.selectInTree();
				return true;
			}
			return false;
		}

	}
*/

}